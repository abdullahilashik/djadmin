from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    phone = models.CharField(max_length=30)

    def __str__(self):
        return self.username


class Category(models.Model):
    title = models.CharField(max_length=250)
    slug = models.CharField(max_length=250)
    user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True)
    created = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.title

    # def num_of_products(self):
    #     return self.product_set.count()

    class Meta:
        ordering = ['-created']
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'


class Product(models.Model):
    title = models.CharField(max_length=250)
    slug = models.CharField(max_length=250)
    description = models.TextField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-created']
        verbose_name = 'Product'
        verbose_name_plural = 'Products'
