from django.contrib import admin
from .models import Category, Product, CustomUser
from django.contrib.auth.models import Group
from django.db.models import Count
from django.http import HttpResponse, HttpResponseRedirect
import csv
from django.urls import path
from django import forms
from django.shortcuts import render, redirect
from django.shortcuts import render

# unregister default model
admin.site.unregister(Group)


class CSVImportForm(forms.Form):
    csv_form = forms.FileField()


class ExportCSVMixin:
    def export_selected(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment;filename={}.csv'.format(meta)

        writer = csv.writer(response)
        writer.writerow(field_names)

        for obj in queryset:
            writer.writerow([getattr(obj, field) for field in field_names])

        return response


@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin, ExportCSVMixin):
    actions = ['export_selected']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin, ExportCSVMixin):
    change_list_template = 'admin/entities/import_button.html'
    actions = ['export_selected']
    list_display = ['title', 'user', 'num_of_products', 'created']
    list_filter = ['created']

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.annotate(
            products=Count('product', distinct=True)
        )
        return queryset

    def num_of_products(self, obj):
        return obj.products

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def get_urls(self):
        print('inside get urls')
        urls = super().get_urls()
        my_urls = [
            path('delete-products/', self.delete_products),
            path('import-csv/', self.import_csv)
        ]
        return my_urls + urls

    def delete_products(self, request):
        print('inside delete products function')
        categories = self.model.objects.all()
        print(f'Found {len(categories)} categories!')
        for item in categories:
            # print(item)
            Product.objects.filter(category=item).delete()
        self.message_user(request, 'Successfully removed products!')
        return HttpResponseRedirect('../')

    def import_csv(self, request):
        if request.method == 'POST':
            csv_file = request.FILES['csv_file']
            reader = csv.reader(csv_file)
            for item in reader:
                print(reader)

        form = CSVImportForm()
        payload = {'form': form}
        return render(request, 'admin/upload_csv.html', payload)

    num_of_products.admin_order_field = 'products'


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin, ExportCSVMixin):
    actions = ['export_selected']
